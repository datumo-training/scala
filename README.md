# Scala workshops

## Common problems

### Unable to load a Suite class that was discovered in the runpath

If you face issue with `java.lang.RuntimeException: Unable to load a Suite class that was discovered in the runpath`
try invalidate Intellij cache: `File -> Invalidate Caches/Restart...`

## Before you begin
You should not change methods already implemented methods.
All places where implementation is missing are marked by `TODO` comment.

## Briefing end
Good luck!
