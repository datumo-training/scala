name := "scala-advanced"

version := "0.1.0"

scalaVersion := "2.13.4"

libraryDependencies ++= Seq(
  "joda-time" % "joda-time" % "2.10.8",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.2.3" % Test
)
