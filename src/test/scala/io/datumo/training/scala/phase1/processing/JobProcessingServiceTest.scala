package io.datumo.training.scala.phase1.processing

import org.joda.time.DateTime
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class JobProcessingServiceTest extends AnyFlatSpec with Matchers {

  private val jobCreationTimestamp: ComputationResult = new DateTime(2020, 6, 1, 13, 0, 0).getMillis

  private val newJob: JobDetail = JobDetail(
    jobId = 1,
    createdAt = jobCreationTimestamp,
    triggerBy = "admin",
  )

  "JobProcessingService" should "handle job status changes" in {
    val startedJob = JobProcessingService.startJob(newJob)

    startedJob shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = false,
      status = Running,
      attempt = 1,
      outputValue = None
    )
    startedJob shouldNot be(newJob)

    val canceledJob = JobProcessingService.cancelJob(startedJob)
    canceledJob shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = true,
      status = Failed,
      attempt = 1,
      outputValue = Some(-1)
    )
    canceledJob shouldNot be(startedJob)


    val restartedJob = JobProcessingService.restartJob(canceledJob)
    restartedJob shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = false,
      status = Running,
      attempt = 2,
      outputValue = None
    )
    restartedJob shouldNot be(canceledJob)


    val canceledJobAttempt2 = JobProcessingService.cancelJob(restartedJob)
    canceledJobAttempt2 shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = true,
      status = Failed,
      attempt = 2,
      outputValue = Some(-1)
    )
    canceledJobAttempt2 shouldNot be(restartedJob)

    val restartedJobAttempt2 = JobProcessingService.restartJob(canceledJobAttempt2)
    restartedJobAttempt2 shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = false,
      status = Running,
      attempt = 3,
      outputValue = None
    )
    restartedJobAttempt2 shouldNot be(canceledJobAttempt2)


    val finishedJob = JobProcessingService.finishJob(restartedJobAttempt2, 123)
    finishedJob shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = true,
      status = Finished,
      attempt = 3,
      outputValue = Option(123L)
    )

    finishedJob shouldNot be(restartedJobAttempt2)
  }

  it should "work in test case with function composition" in {
    val processing = (JobProcessingService.startJob _)
      .andThen(JobProcessingService.cancelJob)
      .andThen(JobProcessingService.restartJob)
      .andThen(JobProcessingService.cancelJob)
      .andThen(JobProcessingService.restartJob)
      .andThen(JobProcessingService.finishJob(_, 123L))

    processing(newJob) shouldBe JobDetail(
      jobId = 1,
      createdAt = jobCreationTimestamp,
      triggerBy = "admin",
      isFinished = true,
      status = Finished,
      attempt = 3,
      outputValue = Option(123L)
    )
  }
}
