package io.datumo.training.scala.phase1.cars

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CarOrderTest extends AnyFlatSpec with Matchers {

  "CarOrder apply method" should "correctly create order based on manufacturer" in {
    CarOrder.apply("bmw", "x5") shouldBe BmwCarOrder("Bmw", "x5", 106000)
    CarOrder.apply("bmw", "m5") shouldBe BmwCarOrder("Bmw", "m5", 380000)
    CarOrder.apply("mercedes", "cla") shouldBe MercedesCarOrder("Mercedes", "cla", 139000)
    CarOrder.apply("mercedes", "amg gt") shouldBe MercedesCarOrder("Mercedes", "amg gt", 512000)

    the [IllegalArgumentException] thrownBy CarOrder.apply("audi", "a8") should have message "Our company does not sell Audi a8 car"
    the [IllegalArgumentException] thrownBy CarOrder.apply("opel", "insignia") should have message "Our company does not sell Opel insignia car"
    the [IllegalArgumentException] thrownBy CarOrder.apply("bmw", "x3") should have message "Our company does not sell Bmw x3 car"
    the [IllegalArgumentException] thrownBy CarOrder.apply("mercedes", "cls") should have message "Our company does not sell Mercedes cls car"
  }
}