package io.datumo.training.scala.phase1.notifications

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks._

class NotificationServiceTest extends AnyFlatSpec with Matchers {

  "NotificationService notifyDevice method" should "send pushNotification when notifying Phone with osVersion >= 5" in {
    val samsungS9 = Phone("1", 9)
    val samsungS4 = Phone("1", 5)
    NotificationService.notifyDevice(samsungS9) shouldBe PushNotification(samsungS9.deviceId)
    NotificationService.notifyDevice(samsungS4) shouldBe PushNotification(samsungS4.deviceId)
  }

  it should "send smsNotification when notifying Phone with osVersion < 5" in {
    val samsungS3 = Phone("2", 4)
    NotificationService.notifyDevice(samsungS3) shouldBe SmsNotification(samsungS3.deviceId)
  }

  it should "correctly send computer users notifications" in {
    val testCases = Table(
      ("device", "notification"),
      (Computer("3", "dev@datumo.pl", Windows, None), WindowsNotification("dev@datumo.pl")),
      (Computer("3", "dev@datumo.pl", MacOs, Some("macUser")), MacOsNotification("macUser")),
      (Computer("4", "dev@datumo.pl", Debian, None), EmailNotification("dev@datumo.pl")),
      (Computer("5", "dev@datumo.pl", Ubuntu, None), EmailNotification("dev@datumo.pl")),
      (Computer("6", "dev@datumo.pl", Solaris, None), NullNotification())
    )

    forAll(testCases) { (device: Device, notification: Notification) =>
      NotificationService.notifyDevice(device) shouldBe notification
    }
  }

  it should "raise exception when device is not associated with AppleId" in {
    val macOsDevice = Computer("3", "dev@datumo.pl", MacOs, None)

    the[IllegalStateException] thrownBy NotificationService.notifyDevice(macOsDevice) should have message
      "AppleId is required for MacOs users"
  }

  it should "send xboxNotification when notifying Xbox" in {
    val xbox = Xbox("4", "ninja")
    NotificationService.notifyDevice(xbox) shouldBe XboxLiveNotification(xbox.xboxLiveId)
  }
}
