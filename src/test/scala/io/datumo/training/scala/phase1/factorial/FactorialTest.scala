package io.datumo.training.scala.phase1.factorial

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class FactorialTest extends AnyFlatSpec with Matchers {

  "PartTwo forLoopFactorial" should "calculate factorial of number" in {
    the[IllegalArgumentException] thrownBy Factorial.forLoopFactorial(-5) should have message "Factorial can be calculated only for non-negative integers"
    Factorial.forLoopFactorial(0) shouldBe 1
    Factorial.forLoopFactorial(3) shouldBe 6
  }

  "PartTwo recursiveFactorial" should "calculate factorial of number" in {
    the[IllegalArgumentException] thrownBy Factorial.recursiveFactorial(-5) should have message "Factorial can be calculated only for non-negative integers"
    Factorial.recursiveFactorial(0) shouldBe 1
    Factorial.recursiveFactorial(3) shouldBe 6
  }
}
