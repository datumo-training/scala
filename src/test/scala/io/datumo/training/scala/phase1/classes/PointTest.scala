package io.datumo.training.scala.phase1.classes

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PointTest extends AnyFlatSpec with Matchers {

  private case class TestPoint(x: Int, y: Int)

  "Point toString method" should "should string in defined format" in {
    val point1 = new Point(1, 5)
    val point2 = new Point(5, 1)
    point1.toString shouldBe "MyCustomPoint(1, 5)"
    point2.toString shouldBe "MyCustomPoint(5, 1)"
  }

  "Point equals method" should "return true when objects have equals fields otherwise e" in {
    val point1 = new Point(1, 5)
    val point2 = new Point(1, 5)
    val point3 = new Point(5, 1)
    val point4 = TestPoint(1, 5)
    point1 shouldBe point2
    point1 shouldNot be(point3)
    point2 shouldNot be(point3)
    point1 shouldNot be(point4)
  }
}
