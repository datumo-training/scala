package io.datumo.training.scala.phase2.orders

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OrderServiceTest extends AnyFlatSpec with Matchers {

  "OrderService meanSoldItemPrice" should "calculate mean sold item price" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )
    OrderService.averageSoldItemPrice(orders) shouldBe 33.75
  }

  "OrderService medianSoldItemPrice" should "calculate median of sold item prices" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders1: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )
    val orders2: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3))),
      Order(2, Seq(items(1)))
    )

    OrderService.medianSoldItemPrice(orders1) shouldBe 35
    OrderService.medianSoldItemPrice(orders2) shouldBe 15
  }

  "OrderService differencesBetweenMedianAndRealValueOfSoldItemPrice" should
    "calculate differences between median and values of sold item price Returned Seq should be sorted" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders1: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )
    val orders2: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3))),
      Order(2, Seq(items(1)))
    )

    OrderService.differencesBetweenMedianAndRealValueOfSoldItemPrice(orders1) shouldBe Seq(-25, -20, 20, 20)
    OrderService.differencesBetweenMedianAndRealValueOfSoldItemPrice(orders2) shouldBe Seq(-5, 0, 0, 40, 40)
  }
}
