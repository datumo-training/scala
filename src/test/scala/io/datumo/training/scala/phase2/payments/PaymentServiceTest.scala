package io.datumo.training.scala.phase2.payments

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PaymentServiceTest extends AnyFlatSpec with Matchers with BeforeAndAfterAll {

  private val paymentRepositoryFacade = PaymentRepositoryFacade()
  private val paymentService = PaymentService(paymentRepositoryFacade)

  override protected def beforeAll(): Unit = {
    paymentRepositoryFacade.addPayments(PaymentData.payments)
  }

  "PaymentService transformToMap method" should "transform seq of payments into Map indexed by id" in {

    paymentService.transformToMap() shouldBe Map(
      PaymentData.payments.head.id -> PaymentData.payments.head,
      PaymentData.payments(1).id -> PaymentData.payments(1),
      PaymentData.payments(2).id -> PaymentData.payments(2),
      PaymentData.payments(3).id -> PaymentData.payments(3),
      PaymentData.payments(4).id -> PaymentData.payments(4),
      PaymentData.payments(5).id -> PaymentData.payments(5),
      PaymentData.payments(6).id -> PaymentData.payments(6),
      PaymentData.payments(7).id -> PaymentData.payments(7),
      PaymentData.payments(8).id -> PaymentData.payments(8)
    )
  }

  "PaymentService blikPaymentWithoutAuthorization method" should
    "return seq of payments which method was equal to Blik and amount was lower than 50" in {

    paymentService.blikPaymentWithoutAuthorization() shouldBe Seq(
      PaymentData.payments.head,
      PaymentData.payments(3)
    )
  }

  "PaymentService blikPaymentWithoutAuthorizationSummary method" should
    "return total sum of blik payment without authorization (amount lower than 50)" in {
    paymentService.blikPaymentWithoutAuthorizationSummary() shouldBe 36
  }

  "PaymentService aggregateProvision method" should "calculate payment provision" in {
    val provisionFormula: Payment => Double = payment => payment.amount * 0.01
    paymentService.aggregateProvision(provisionFormula) shouldBe 24.57
  }
}
