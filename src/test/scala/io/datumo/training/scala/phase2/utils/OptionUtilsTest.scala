package io.datumo.training.scala.phase2.utils

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OptionUtilsTest extends AnyFlatSpec with Matchers {

  "optionSum" should "sum collection of Int Options" in {
    val seq = Seq(None, Some(2), Some(3), None, Some(2), None)

    OptionUtils.optionSum(seq) shouldBe 7
  }

  "calculateIncome" should "calculate income" in {
    OptionUtils.calculateIncome(None, Some(0.17)) shouldBe 0
    OptionUtils.calculateIncome(None, None) shouldBe 0
    OptionUtils.calculateIncome(Some(1000), None) shouldBe 1000.0
    OptionUtils.calculateIncome(Some(1000), Some(0.17)) shouldBe 830.0
  }
}
