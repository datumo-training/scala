package io.datumo.training.scala.phase2.payments

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PaymentRepositoryFacadeTest extends AnyFlatSpec with Matchers {

  "PaymentRepositoryFacade" should "add payments as Scala collection" in {
    val paymentRepositoryFacade = PaymentRepositoryFacade()

    paymentRepositoryFacade.addPayments(PaymentData.payments)
    paymentRepositoryFacade.fetchAll() shouldBe PaymentData.javaPayments
  }

  it should "allow to fetch records as Scala collection" in {
    val paymentRepositoryFacade = PaymentRepositoryFacade()
    paymentRepositoryFacade.add(PaymentData.javaPayments)

    paymentRepositoryFacade.fetchPayments() shouldBe PaymentData.payments
  }
}
