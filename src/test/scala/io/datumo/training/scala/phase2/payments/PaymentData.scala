package io.datumo.training.scala.phase2.payments

import java.util

object PaymentData {
  val payments: Seq[Payment] = Seq(
    Payment(1, 23, PaymentService.blikPayment),
    Payment(2, 43, "online"),
    Payment(3, 433, "cash"),
    Payment(4, 13, PaymentService.blikPayment),
    Payment(5, 433, "online"),
    Payment(6, 433, "cash"),
    Payment(7, 223, PaymentService.blikPayment),
    Payment(8, 423, "online"),
    Payment(9, 433, "cash")
  )

  val javaPayments: java.util.List[Payment] = {
    val list = new util.LinkedList[Payment]()
    payments.foreach(list.add)
    list
  }
}
