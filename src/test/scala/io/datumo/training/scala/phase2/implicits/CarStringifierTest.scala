package io.datumo.training.scala.phase2.implicits

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CarStringifierTest extends AnyFlatSpec with Matchers {

  "optionStringToString" should "properly convert Option[String] to String" in {
    Implicits.optionAtoString[String](Some("Beer")) shouldEqual "Beer"
  }

  it should "return Unknown string when None" in {
    Implicits.optionAtoString[String](None) shouldEqual "Unknown"
  }

  it should "properly convert Option[Int] to String" in {
    Implicits.optionAtoString[Int](Some(2077)) shouldEqual "2077"
  }

  it should "properly convert Option[Double] to String" in {
    Implicits.optionAtoString[Double](Some(420.69)) shouldEqual "420.69"
  }

  it should "properly convert Option[Boolean] to String" in {
    Implicits.optionAtoString[Boolean](Some(true)) shouldEqual "true"
  }

  "CarStringifier" should "properly convert class" in {
    val car = Car("Renault", Some("Captur"), Some(2017), Some(1.2), None)
    val carStringified = StringifiedCar("Renault", "Captur", "2017", "1.2", "Unknown")

    CarStringifier.stringify(car) shouldEqual carStringified
  }
}