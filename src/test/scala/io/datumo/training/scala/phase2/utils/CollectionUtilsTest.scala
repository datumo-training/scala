package io.datumo.training.scala.phase2.utils

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CollectionUtilsTest extends AnyFlatSpec with Matchers {

  "removeDuplicates method" should "removes duplicates from collections" in {
    val seq1 = Seq(1, 2, 3, 1, 2, 3, 3, 2, 1, 4)
    val seq2 = Seq(9, 2, 3, 9, 4, 3, 4)

    CollectionUtils.removeDuplicates(seq1) shouldBe Set(1, 2, 3, 4)
    CollectionUtils.removeDuplicates(seq2) shouldBe Set(2, 3, 4, 9)
  }

  "Mean method" should "calculate mean" in {
    val col: Seq[Int] = Seq(1, 2, 4, 6, 12, 14)

    CollectionUtils.mean(col) shouldBe 6.5
    the [IllegalArgumentException] thrownBy CollectionUtils.mean(Seq.empty) should have message "mean cannot be calculated for empty collection"
  }

  "cartesianProduct method" should "return cartesian product of two collections" in {
    val xs = Seq(Some(5), None, Some(9))
    val ys = Seq(None, None, Some(12))

    val expectedResult = Seq(
      (Some(5), None), (Some(5), None), (Some(5), Some(12)),
      (None, None), (None, None), (None, Some(12)),
      (Some(9), None), (Some(9), None), (Some(9), Some(12)),
    )

    CollectionUtils.cartesianProduct(xs, ys) shouldBe expectedResult
  }

  "evenNaturalNumbersBetween" should "return even natural numbers between range" in {
    CollectionUtils.evenNaturalNumbersBetween(1, 1) shouldBe Seq.empty
    CollectionUtils.evenNaturalNumbersBetween(1, 3) shouldBe Seq(2)
    CollectionUtils.evenNaturalNumbersBetween(1, 12) shouldBe Seq(2, 4, 6, 8, 10, 12)
  }

  "Quicksort method" should "sort collection" in {
    val col1 = List(9, 9, 3, 6, 2, 1, 5, 2, 1)
    val col2 = List(9, 3, 6, 2, 5, 1)

    CollectionUtils.quickSort(col1) shouldBe List(1, 1, 2, 2, 3, 5, 6, 9, 9)
    CollectionUtils.quickSort(col2) shouldBe List(1, 2, 3, 5, 6, 9)
  }
}
