package io.datumo.training.scala.phase2.subscriptions

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class SubscriptionServiceTest extends AnyFlatSpec with Matchers {

  "SubscriptionService getSubscriptionTypeByUserId method" should "return user subscription type" in {
    val userRepository = UserRepository(
      Map(
        1 -> User(1, "Altair", 12),
        2 -> User(2, "Ezio", 15),
        3 -> User(3, "Connor", 17),
        4 -> User(4, "Edward", 18),
        5 -> User(5, "Arno", 20)
      )
    )

    val subscriptionRepository = SubscriptionRepository(
      Map(
        12 -> Subscription(12, SilverSubscriptionType),
        15 -> Subscription(15, GoldSubscriptionType),
        17 -> Subscription(17, BronzeSubscriptionType),
        18 -> Subscription(18, SilverSubscriptionType),
        20 -> Subscription(20, GoldSubscriptionType)
      )
    )

    val subscriptionService = SubscriptionService(userRepository, subscriptionRepository)

    subscriptionService.getSubscriptionTypeByUserId(1) shouldBe Some(SilverSubscriptionType)
    subscriptionService.getSubscriptionTypeByUserId(2) shouldBe Some(GoldSubscriptionType)
    subscriptionService.getSubscriptionTypeByUserId(7) shouldBe None
  }
}
