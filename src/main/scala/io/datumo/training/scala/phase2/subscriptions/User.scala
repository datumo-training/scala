package io.datumo.training.scala.phase2.subscriptions

case class User(id: Int, name: String, subscriptionId: Int)
