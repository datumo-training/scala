package io.datumo.training.scala.phase2.subscriptions

sealed trait SubscriptionType
case object BronzeSubscriptionType extends SubscriptionType
case object SilverSubscriptionType extends SubscriptionType
case object GoldSubscriptionType extends SubscriptionType

case class Subscription(id: Int, `type`: SubscriptionType)
