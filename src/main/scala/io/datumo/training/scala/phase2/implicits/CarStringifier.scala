package io.datumo.training.scala.phase2.implicits

case class Car(
                brand: String,
                model: Option[String],
                productionYear: Option[Int],
                engineDisplacement: Option[Double],
                isRunning: Option[Boolean]
              )

case class StringifiedCar(
                           brand: String,
                           model: String,
                           productionYear: String,
                           engineDisplacement: String,
                           isRunning: String
                         )

object CarStringifier {

  def stringify(car: Car): StringifiedCar = {
    //TODO: code is not compiling. I guess we forget about something
//    StringifiedCar(
//      car.brand,
//      car.model,
//      car.productionYear,
//      car.engineDisplacement,
//      car.isRunning
//    )
    ???
  }
}
