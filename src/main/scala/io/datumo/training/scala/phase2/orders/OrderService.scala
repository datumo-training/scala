package io.datumo.training.scala.phase2.orders

case class Item(id: Int, price: Int)
case class Order(id: Int, parts: Seq[Item])

object OrderService {

  def averageSoldItemPrice(orders: Seq[Order]): Double = {
    //TODO: like in name. Calculate average sold item price
    ???
  }

  // Median formula https://en.wikipedia.org/wiki/Median
  def medianSoldItemPrice(orders: Seq[Order]): Double = {
    //TODO: calculate median of sold items
    ???
  }

  def differencesBetweenMedianAndRealValueOfSoldItemPrice(orders: Seq[Order]): Seq[Double] = {
    //TODO: calculate differences between median and sold item value
    ???
  }
}
