package io.datumo.training.scala.phase2.payments

case class Payment(id: Int, amount: Int, method: String)
