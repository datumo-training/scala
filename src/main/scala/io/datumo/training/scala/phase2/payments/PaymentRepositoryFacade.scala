package io.datumo.training.scala.phase2.payments

case class PaymentRepositoryFacade() extends JavaPaymentRepository {

  def fetchPayments(): Seq[Payment] = {
    //TODO: convert Java collection into Scala
    // hint: you can use super to access ancestor methods
    ???
  }

  def addPayments(payments: Seq[Payment]): Unit = {
    //TODO: convert Scala collection into Java
    // hint: you can use super to access ancestor methods
  }
}
