package io.datumo.training.scala.phase2.payments

case class PaymentService(paymentRepositoryFacade: PaymentRepositoryFacade) {

  def transformToMap(): Map[Int, Payment] = {
    //TODO: fetch data from repository and create map indexed by paymentId
    ???
  }

  def blikPaymentWithoutAuthorization(): Seq[Payment] = {
    //TODO: return of payments by Blik with amount lower than 50
    ???
  }

  def blikPaymentWithoutAuthorizationSummary(): Int = {
    //TODO: calculate agregated value of payments by Blik without authorization
    ???
  }

  def aggregateProvision(provisionFormula: Payment => Double): Double = {
    //TODO: each payment has provision. Calculate our provision
    ???
  }
}

object PaymentService {
  val blikPayment = "blik"
}
