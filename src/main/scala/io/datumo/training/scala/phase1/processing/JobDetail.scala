package io.datumo.training.scala.phase1.processing

case class JobDetail(
                      jobId: Int,
                      createdAt: Long,
                      triggerBy: String,
                      isFinished: Boolean = false,
                      status: JobStatus = New,
                      attempt: Int = 0,
                      outputValue: Option[ComputationResult] = None
                    )