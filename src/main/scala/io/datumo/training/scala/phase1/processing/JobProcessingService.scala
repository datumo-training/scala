package io.datumo.training.scala.phase1.processing

object JobProcessingService {

  val cancellationError = "Job cancelled by user"

  def startJob(job: JobDetail): JobDetail = {
    //TODO
    ???
  }

  def restartJob(job: JobDetail): JobDetail = {
    //TODO: don't forget to increment attempt field and restart outputValue
    ???
  }

  def cancelJob(job: JobDetail): JobDetail = {
    //TODO: set outputValue to -1
    ???
  }

  def finishJob(job: JobDetail, returnValue: Long): JobDetail = {
    //TODO: set outputValue
    ???
  }
}
