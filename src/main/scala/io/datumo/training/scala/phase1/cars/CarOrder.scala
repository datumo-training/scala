package io.datumo.training.scala.phase1.cars

abstract class CarOrder(manufacturer: String, model: String, price: Int)

case class BmwCarOrder(manufacturer: String, model: String, price: Int) extends CarOrder(manufacturer, model, price)
case class MercedesCarOrder(manufacturer: String, model: String, price: Int) extends CarOrder(manufacturer, model, price)


object CarOrder {

  private val currentPricing: Map[String, Int] = Map(
    "bmw x5" -> 106000,
    "bmw m5" -> 380000,
    "mercedes cla" -> 139000,
    "mercedes amg gt" -> 512000
  )

  def apply(manufacturer: String, model: String): CarOrder = {
    //TODO: create car order based on manufacture and model.
    // Our company only sells cars included in pricing.
    ???
  }
}