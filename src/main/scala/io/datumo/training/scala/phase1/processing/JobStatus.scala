package io.datumo.training.scala.phase1.processing

sealed trait JobStatus

case object New extends JobStatus
case object Running extends JobStatus
case object Finished extends JobStatus
case object Failed extends JobStatus
