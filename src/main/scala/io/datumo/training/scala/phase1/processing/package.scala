package io.datumo.training.scala.phase1

package object processing {
  type ComputationResult = Long
}
