package io.datumo.training.scala.phase1.factorial

object Factorial {

  //TODO: use for loop
  def forLoopFactorial(n: Int): Int = ???

  //TODO: use recursion and pattern matching
  def recursiveFactorial(n: Int): Int = ???
}
