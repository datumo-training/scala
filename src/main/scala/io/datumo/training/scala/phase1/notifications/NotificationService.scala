package io.datumo.training.scala.phase1.notifications

import com.typesafe.scalalogging.LazyLogging

sealed trait OsType
case object Windows extends OsType
case object MacOs extends OsType
case object Debian extends OsType
case object Ubuntu extends OsType
case object Solaris extends OsType

sealed abstract class Device
case class Phone(deviceId: String, appVersion: Int) extends Device
case class Computer(deviceId: String, email: String, osName: OsType, appleId: Option[String]) extends Device
case class Xbox(deviceId: String, xboxLiveId: String) extends Device

sealed abstract class Notification
case class PushNotification(deviceId: String) extends Notification
case class EmailNotification(email: String) extends Notification
case class SmsNotification(deviceId: String) extends Notification
case class XboxLiveNotification(xboxLiveId: String) extends Notification
case class WindowsNotification(email: String) extends Notification
case class MacOsNotification(appleId: String) extends Notification
case class NullNotification() extends Notification


object NotificationService extends LazyLogging {
  def notifyDevice(device: Device): Notification = {
    //TODO: implement user notification. Our testing team made grate job with test cases.
    // It's like documentation.
    ???
  }
}
