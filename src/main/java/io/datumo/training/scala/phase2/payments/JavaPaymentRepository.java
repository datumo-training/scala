package io.datumo.training.scala.phase2.payments;

import java.util.LinkedList;
import java.util.List;

public class JavaPaymentRepository {

    final private List<Payment> db = new LinkedList<>();

    protected List<Payment> fetchAll() {
        return this.db;
    }

    protected void add(List<Payment> payments) {
        this.db.addAll(payments);
    }
}

